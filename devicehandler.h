/***************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the QtBluetooth module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#ifndef DEVICEHANDLER_H
#define DEVICEHANDLER_H

#include "bluetoothbaseclass.h"
#include "myJson.h"


#include <QDateTime>
#include <QVector>
#include <QTimer>
#include <QLowEnergyController>
#include <QLowEnergyService>

class DeviceInfo;
class MyJson;

class DeviceHandler : public BluetoothBaseClass
{
    Q_OBJECT
    Q_PROPERTY(AddressType addressType READ addressType WRITE setAddressType)
    Q_PROPERTY(QString deviceID READ deviceID NOTIFY deviceIDChangeed)
    Q_PROPERTY(bool connnected READ connnected NOTIFY connectedChangeed)
    Q_PROPERTY(QString jsonString READ jsonString WRITE setjsonString NOTIFY jsonStringChanged)
    Q_PROPERTY(int grade1_Content READ grade1_Content NOTIFY mycontentChanged)
    Q_PROPERTY(int grade2_Content READ grade2_Content  NOTIFY mycontentChanged)
    Q_PROPERTY(int music_Content READ music_Content  NOTIFY mycontentChanged)
    //Q_PROPERTY(QVariant mycontent READ mycontent NOTIFY mycontentChanged)

public:
    enum class AddressType {
        PublicAddress,
        RandomAddress
    };
    Q_ENUM(AddressType)

    DeviceHandler(QObject *parent = 0);

    void setDevice(DeviceInfo *device);
    void setAddressType(AddressType type);
    void setjsonString(QString json);

    QString jsonString() const;
    AddressType addressType() const;
    bool connnected() const;
    int grade1_Content() const;
    int grade2_Content() const;
    int music_Content() const;
    QString deviceID() const;
    //QVariant &mycontent() const;

signals:
    void addrxlog(QVariant log);
    void jsonStringChanged();
    void connectedChangeed();
    void mycontentChanged();
    void deviceIDChangeed();

public slots:
    void disconnectService(const bool val);
    void sendBLEData(const QByteArray &value);
    void sendWJSContracted(const bool val);
    void sendWJSContracted(const QString val);
    void sendBLEBinaryData();
    void jsonRefresh();
    void jsonUpdate();
    void JsonRecvDone();
    void setDeviceID(const QString id);

private slots:
    //QLowEnergyController
    void serviceDiscovered(const QBluetoothUuid &);
    void serviceScanDone();

    //QLowEnergyService
    void serviceStateChanged(QLowEnergyService::ServiceState s);
    void updateHeartRateValue(const QLowEnergyCharacteristic &c, const QByteArray &value);
    void confirmedDescriptorWrite(const QLowEnergyDescriptor &d, const QByteArray &value);
    void characteristicWritten(const QLowEnergyCharacteristic &d, const QByteArray &newValue);
    void timeout();
    void delayCommandTimeout();
private:

    QLowEnergyController *m_control;
    QLowEnergyService *m_service;
    QLowEnergyDescriptor m_notificationDesc;
    DeviceInfo *m_currentDevice;
    MyJson *m_myJson;

    bool m_foundService;
    QLowEnergyController::RemoteAddressType m_addressType = QLowEnergyController::PublicAddress;

    unsigned int dateMaxSize;
    unsigned int writtenCnt;
    unsigned int SendedCnt;
    QTimer *timer;
    QTimer *delayCommandTimer;
    QByteArray blob;
    QString m_jsonString;
    QString m_deviceId;
    bool firstTime;

    int content[3];

};

#endif // DEVICEHANDLER_H
