TEMPLATE = app
TARGET = TaptiloStore

QT += qml quick bluetooth widgets
CONFIG += c++11

HEADERS += \
    connectionhandler.h \
    deviceinfo.h \
    devicefinder.h \
    devicehandler.h \
    bluetoothbaseclass.h \
    heartrate-global.h \
    myJson.h

SOURCES += main.cpp \
    connectionhandler.cpp \
    deviceinfo.cpp \
    devicefinder.cpp \
    devicehandler.cpp \
    bluetoothbaseclass.cpp \
    myJson.cpp

RESOURCES += qml.qrc \
    images.qrc \
    binary.qrc

ICON = assets/icon.icns
RC_ICONS = assets/icon.ico

#QT_QUICK_CONTROLS_STYLE = imagine
#QT_QUICK_CONTROLS_STYLE=universal ./app
# Additional import path used to resolve QML modules in Qt Creator's code model
#QML_IMPORT_PATH =

#target.path = $$[QT_INSTALL_EXAMPLES]/bluetooth/heartrate-game
INSTALLS += target



ios {
    ios_icon.files = $$files($$PWD/ios/Icon-App-*.png)
    QMAKE_BUNDLE_DATA += ios_icon

    itunes_icon.files = $$files($$PWD/ios/ITunesArtwork*)
    QMAKE_BUNDLE_DATA += itunes_icon

    app_launch_images.files = $$PWD/ios/LauncherScreen.xib $$files($$PWD/ios/LaunchImage*.png) $$files($$PWD/ios/splash_*.png)
    QMAKE_BUNDLE_DATA += app_launch_images

    QMAKE_INFO_PLIST = $$PWD/ios/Info.plist

}

DISTFILES += \
    myJson
