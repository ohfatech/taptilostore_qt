/***************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the QtBluetooth module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.5
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

//import QtQuick.Window 2.2
import Qt.labs.platform 1.0

//import QtQuick.Controls.Styles 1.4
import "."

Item {

    property int grade1 : deviceHandler.grade1_Content
    property int grade2 : deviceHandler.grade2_Content
    property int music : deviceHandler.music_Content

    function update(){
        busyIndicator.visible = false

        if(grade1 === 3)
        {
            uncontractedIcon.spin = true
        }else{
            uncontractedIcon.spin = false
        }

        if(grade2 === 3)
        {
            uebIcon.spin = true
        }else{
            uebIcon.spin = false
        }

        if(music === 3)
        {
            musicIcon.spin = true
        }else{
            musicIcon.spin = false
        }



        musicIcon.rotation = 0
        uebIcon.rotation = 0
        uncontractedIcon.rotation = 0

        //console.log("update")
        if(grade1 === 0)
            uncontractedIcon.source = "qrc:/qml/images/cellUncheck.png"
        else if(grade1 === 1)
            uncontractedIcon.source = "qrc:/qml/images/cellCheck.png"
        else if(grade1 === 2)
            uncontractedIcon.source = "qrc:/qml/images/buttonBackground.png"
        else if(grade1 === 3)
            uncontractedIcon.source = "qrc:/qml/images/spinner.png"
/*
        if(grade1 === 3)
        {
            busyIndicator.visible = true
        }else{
            busyIndicator.visible = false
        }
*/
        if(grade2 === 0)
            uebIcon.source = "qrc:/qml/images/cellUncheck.png"
        else if(grade2 === 1)
            uebIcon.source = "qrc:/qml/images/cellCheck.png"
        else if(grade2 === 2)
            uebIcon.source = "qrc:/qml/images/buttonBackground.png"
        else if(grade2 === 3){
            uebIcon.source = "qrc:/qml/images/spinner.png"
        }

        if(music === 0)
            musicIcon.source = "qrc:/qml/images/cellUncheck.png"
        else if(music === 1)
            musicIcon.source = "qrc:/qml/images/cellCheck.png"
        else if(music === 2)
            musicIcon.source = "qrc:/qml/images/buttonBackground.png"
        else if(music === 3)
            musicIcon.source = "qrc:/qml/images/spinner.png"



    }

    onGrade1Changed: update()
    onGrade2Changed: update()
    onMusicChanged: update()

    onVisibleChanged: {
        busyIndicator.visible = false;
    }

    ColumnLayout {
        id: manageContents
        anchors.fill: parent
        spacing: 0

        Text{
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: 10
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter
            font.bold: true
            font.pixelSize: DesignSettings.mediumFontSize
            color: DesignSettings.textColor
            text: qsTr("My Content")
            height: 50
        }

        Rectangle{
            anchors.left: parent.left
            anchors.right: parent.right
            height: 50
            color: "transparent"
            Rectangle{
                anchors.fill: parent
                color: "white"
                opacity: 0.1
            }
            BottomLine {
                height: 1;
                width: parent.width
                anchors.left: parent.left
                anchors.leftMargin: 10
                color: "#c8c7cc"
            }

           Text{
                id: uncontractedLabel
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.leftMargin: 10
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                text: qsTr("Uncontracted Braille (Grade 1)")
                color: DesignSettings.textColor
                font.pixelSize: DesignSettings.smallTinyFontSize
            }
            Image{
                id: uncontractedIcon
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.rightMargin: 10
                anchors.topMargin: 10
                anchors.bottomMargin: 10

                horizontalAlignment: Image.AlignHCenter
                verticalAlignment: Image.AlignVCenter
                source: "qrc:/qml/images/cellCheck.png"

                fillMode: Image.PreserveAspectFit
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                property bool spin: false
                RotationAnimator on rotation {
                    from: 0;
                    to: 360;
                    loops: Animation.Infinite
                    duration: 500
                    running: uncontractedIcon.spin
                }

            }
            MouseArea{
                id: uncontractedBtn
                anchors.fill: parent
                function send(){
                    if(grade1 === 1)
                        return;

                    deviceHandler.sendBLEData("#CP en_us#")
                    deviceHandler.sendWJSContracted(false);
                    deviceHandler.sendBLEData("#SC reboot#")
                    msg.open();

                    //console.log(items.length)

                }
                onPressed: {
                    uncontractedIcon.opacity = 0.7
                    uncontractedIcon.scale = 0.8
                    send();
                }
                onReleased: {
                    uncontractedIcon.opacity = 1
                    uncontractedIcon.scale = 1
                }

                Accessible.role: Accessible.Button
                Accessible.name: uncontractedLabel.text
                Accessible.description: "Select to device Content"
                Accessible.onPressAction: uncontractedBtn.send()

            }
        }
        Rectangle {
            anchors.left: parent.left
            anchors.right: parent.right
            height: 50
            color: "transparent"
            Rectangle{
                anchors.fill: parent
                color: "white"
                opacity: 0.1
            }
            BottomLine {
                height: 1;
                width: parent.width
                anchors.left: parent.left
                anchors.leftMargin: 10
                color: "#c8c7cc"
            }

            Text{
                id: usbLabel
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.leftMargin: 10
                font.pixelSize: DesignSettings.smallTinyFontSize
                color: DesignSettings.textColor
                horizontalAlignment: Image.AlignHCenter
                verticalAlignment: Image.AlignVCenter

                text: qsTr("Unified English Braille (Grade 2)")
            }

            Image{
                id: uebIcon
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.rightMargin: 10
                anchors.bottom: parent.bottom
                //height: 30
                rotation: 0
                anchors.topMargin: 10
                anchors.bottomMargin: 10
                source: "qrc:/qml/images/cellUncheck.png"
                fillMode: Image.PreserveAspectFit
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                property bool spin: false
                RotationAnimator on rotation {
                    from: 0;
                    to: 360;
                    loops: Animation.Infinite
                    duration: 500
                    running: uebIcon.spin
                }

            }
            MouseArea{
                id: usbBtn
                anchors.fill: parent
                function send(){
                    if(grade2 === 1)
                        return

                    deviceHandler.sendBLEData("#CP en_us#")
                    deviceHandler.sendWJSContracted(true);
                    deviceHandler.sendBLEData("#SC reboot#")
                    //
                    //deviceHandler.sendBLEData("#SC reboot#")
                    msg.open();
                }
                onPressed: {
                    uebIcon.opacity = 0.7
                    uebIcon.scale = 0.8
                    send();
                }
                onReleased: {
                    uebIcon.opacity = 1
                    uebIcon.scale = 1
                }
            }
            Accessible.role: Accessible.Button
            Accessible.name: usbLabel.text
            Accessible.description: "Select to device Content"
            Accessible.onPressAction: usbBtn.send()
        }
        Rectangle {
            anchors.left: parent.left
            anchors.right: parent.right
            height: 50
            color: "transparent"
            Rectangle{
                anchors.fill: parent
                color: "white"
                opacity: 0.1
            }

            Text{
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.leftMargin: 10
                horizontalAlignment: Image.AlignHCenter
                verticalAlignment: Image.AlignVCenter
                font.pixelSize: DesignSettings.smallTinyFontSize
                color: DesignSettings.textColor
                text: qsTr("Braille Music (Beta ver.)")
            }

            Image{
                id: musicIcon
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.rightMargin: 10

                anchors.topMargin: 10
                anchors.bottomMargin: 10
                source: "qrc:/qml/images/buttonBackground.png"
                fillMode: Image.PreserveAspectFit
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                property bool spin: false
                RotationAnimator on rotation {
                    from: 0;
                    to: 360;
                    loops: Animation.Infinite
                    duration: 500
                    running: musicIcon.spin
                }
            }
            MouseArea{
                id: musicBtn
                anchors.fill: parent
                function send(){
                    if(music === 2)
                    {
                        //console.log("busy")
                        deviceHandler.sendWJSContracted("music_en_us");


                    }else if(music == 0){

                        deviceHandler.sendBLEData("#CP music_en_us#")
                        deviceHandler.sendBLEData("#SC reboot#")
                        msg.open();
                    }
                    else if(music === 1){
                        return
                    }

                }
                onPressed: {
                    musicIcon.opacity = 0.7
                    musicIcon.scale = 0.8
                    send();
                }
                onReleased: {
                    musicIcon.opacity = 1
                    musicIcon.scale = 1
                }
            }
            Accessible.role: Accessible.Button
            Accessible.name: usbLabel.text
            Accessible.description: "Select to device Content"
            Accessible.onPressAction: musicBtn.send()
        }
        /*
        Text{
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            anchors.left: parent.left
            anchors.right: parent.right
            font.pixelSize: DesignSettings.mediumFontSize
            color: DesignSettings.textColor
            text: qsTr("loading")
        }
        ProgressBar {
         value: 0.5
         Layout.fillWidth: true
         anchors.left: parent.left
         anchors.right: parent.right
         anchors.leftMargin: 20
         anchors.rightMargin: 20
         height: 10
         Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
         //Layout.fillHeight: true
         //Layout.fillHeight: true
        }*/
        Item { Layout.fillHeight: true }
    }
    Image {
        id: busyIndicator
        source: "qrc:/qml/images/spinner.png"
        anchors.fill: parent
        anchors.rightMargin: 120
        anchors.leftMargin: 120
        fillMode: Image.PreserveAspectFit
        //Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        RotationAnimator on rotation {
            from: 0;
            to: 360;
            loops: Animation.Infinite
            duration: 800
            running: true
        }

    }

    MessageDialog {
       id: msg
       buttons: MessageDialog.Ok
       text: "To apply the new settings,\nTaptilo will restart."
       onVisibleChanged: {
           musicIcon.opacity = 1
           musicIcon.scale = 1
           uebIcon.opacity = 1
           uebIcon.scale = 1
           uncontractedIcon.opacity = 1
           uncontractedIcon.scale = 1
       }

       onOkClicked: {
            this.close();
           busyIndicator.visible = true
           //viewDeviceDetail = false;
       }
   }





}
