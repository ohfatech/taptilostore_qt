/***************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the QtBluetooth module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

import QtQuick 2.5
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import Shared 1.0



Item {
    id: viewSearch
    anchors.fill: parent
    Accessible.ignored: true
    Rectangle{
        anchors.fill: parent
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#003558" }
            GradientStop { position: 1.0; color: "#0080cd" }
        }
    }
    onVisibleChanged: {
        if(viewSearch.visible)
            deviceFinder.startSearch()
    }
    Component.onCompleted: deviceFinder.startSearch()

    Rectangle {
        id: viewContainer
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.topMargin: DesignSettings.fieldMargin * 0.5
        anchors.bottomMargin: DesignSettings.fieldMargin
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width - DesignSettings.fieldMargin*2
        color: "transparent"
        radius: DesignSettings.buttonRadius

        Text {
            id: title
            width: parent.width
            height: DesignSettings.fieldHeight
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: DesignSettings.textColor
            font.pixelSize: DesignSettings.mediumFontSize
            text: qsTr("Select a device")

            Accessible.role: Accessible.StaticText
            Accessible.name: title.text
            Accessible.description: "title"

            BottomLine {
                height: 1;

                //width: parent.width + DesignSettings.fieldMargin*2
                //anchors.left: viewSearch.left
                //anchors.right: viewSearch.right
                //width: viewSearch.width
                color: "#898989"
            }
        }


        ListView {
            id: devices
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.top: title.bottom
            anchors.topMargin: 10
            model: deviceFinder.devices
            clip: true

            spacing: 10

            delegate: Rectangle {
                id: box
                height:DesignSettings.fieldHeight * 1.2
                width: parent.width
                opacity: 0.3
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                radius: 10

                function connectToDevice(){
                    deviceFinder.connectToService(modelData.deviceAddress);
                    box.opacity = 0.6
                    app.deviceId = devicelabel.text
                }

                MouseArea {
                    id: device
                    anchors.fill: parent
                    onClicked: box.connectToDevice()
                }

                Text {
                    id: devicelabel
                    font.pixelSize: DesignSettings.smallFontSize
                    anchors.centerIn: parent
                    text: modelData.deviceName
                    color: "white"
                }

                Accessible.role: Accessible.Button
                Accessible.name: devicelabel.text
                Accessible.description: "Connect to taptilo device"
                Accessible.onPressAction: box.connectToDevice()
            }
        }
    }

}
