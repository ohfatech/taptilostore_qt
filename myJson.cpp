/***************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the QtBluetooth module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "myJson.h"
#include <QDebug>
#include <QJsonArray>


MyJson::MyJson(QObject *parent):
  QObject()
{
    m_bJsonRecv = false;

}

MyJson::~MyJson()
{
}


bool MyJson::loadJSONforString(const QByteArray value){
    int ind;

    if(!m_bJsonRecv){
        ind = value.indexOf("#RJS ");
        if(ind != -1)
            m_bJsonRecv = true;
        else
            return false;
        row_data.append(value.mid(ind + 5));
    }else{
       if(value.endsWith("}#")){
           m_bJsonRecv = false;
           row_data.append(value.left(value.length()-1));
           emit JsonRecvDone();
       }else{
           row_data.append(value.left(value.length()));
       }
    }
    return false;
}


QByteArray MyJson::getJsonData(){
    QJsonParseError error;
    QJsonDocument loadDoc(QJsonDocument::fromJson(row_data,&error));
    //qDebug() << error.errorString();
    return loadDoc.toJson();
}

QByteArray MyJson::getJsonData(const QString iData){

    QJsonParseError error;
    QByteArray tmp;
    tmp.append(iData);
    QJsonDocument loadDoc(QJsonDocument::fromJson(tmp,&error));
    //qDebug() << error.errorString();
    return loadDoc.toJson(QJsonDocument::Compact);

}
int MyJson::getMusicContent(const QString iData){

    QJsonParseError error;
    QByteArray tmp;
    tmp.append(iData);
    QJsonDocument loadDoc(QJsonDocument::fromJson(tmp,&error));
    //qDebug() << error.errorString();
    //qDebug() << tmp;
    int ret = 2;
    QJsonObject root_obj = loadDoc.object();

    QJsonObject primary = root_obj["DeviceInfo"].toObject();
    QJsonArray arr = primary["SupportContent"].toArray();


    for(int i = 0 ; i < arr.size(); i++){
        if(arr.at(i).toString().compare("music_en_us") == 0 ){
            ret = 1;
            break;
        }
    }
    if(ret == 2){
        return ret;
    }

    ret = primary["CurrentContent"].toString().compare("music_en_us");
    if(ret){
        return 0;
    }else
        return 1;
}
int MyJson::getGrade1Content(const QString iData){
    QJsonParseError error;
    QByteArray tmp;
    tmp.append(iData);
    QJsonDocument loadDoc(QJsonDocument::fromJson(tmp,&error));
    //qDebug() << error.errorString();
    //qDebug() << tmp;
    int ret = 2;
    QJsonObject root_obj = loadDoc.object();

    QJsonObject primary = root_obj["DeviceInfo"].toObject();
    QJsonArray arr = primary["SupportContent"].toArray();


    for(int i = 0 ; i < arr.size(); i++){
        if(arr.at(i).toString().compare("en_us") == 0 ){
            ret = 1;
            break;
        }
    }
    if(ret == 2){
        return ret;
    }

    ret = primary["CurrentContent"].toString().compare("en_us");
    if(ret !=  0){
        return 0;
    }

    QJsonObject Contracted = root_obj["en_us"].toObject();
    if(Contracted["Contracted"].toBool()){
        return 0;
    }else{
        return 1;
    }
}

int MyJson::getGrade2Content(const QString iData){
    QJsonParseError error;
    QByteArray tmp;
    tmp.append(iData);
    QJsonDocument loadDoc(QJsonDocument::fromJson(tmp,&error));
    //qDebug() << error.errorString();
    //qDebug() << tmp;
    int ret = 2;
    QJsonObject root_obj = loadDoc.object();

    QJsonObject primary = root_obj["DeviceInfo"].toObject();
    QJsonArray arr = primary["SupportContent"].toArray();


    for(int i = 0 ; i < arr.size(); i++){
        if(arr.at(i).toString().compare("en_us") == 0 ){
            ret = 1;
            break;
        }
    }
    if(ret == 2){
        return ret;
    }

    ret = primary["CurrentContent"].toString().compare("en_us");
    if(ret !=  0){
        return 0;
    }

    QJsonObject Contracted = root_obj["en_us"].toObject();

    if(Contracted["Contracted"].toBool()){
        return 1;
    }else{
        return 0;
    }
}


bool MyJson::loadJSON(){
    row_data = "";
    m_bJsonRecv = false;
}

int MyJson::setJsonData(const QString header, const QString config, const QString data){
    QJsonParseError error;
    QJsonDocument saveDoc;
    QJsonDocument loadDoc(QJsonDocument::fromJson(row_data,&error));
    //qDebug() << error.errorString();
    //qDebug() << tmp;
    QJsonObject root_obj = loadDoc.object();

    QJsonObject primary = root_obj[header].toObject();
    QJsonValue val = primary.value(config);

    if(val.isArray()){
        QJsonArray arr = primary[config].toArray();
        //arr.insert("data");
        arr.append("music_en_us");
        primary.insert(config,arr);
        root_obj.insert(header,primary);
        saveDoc.setObject(root_obj);
        row_data.clear();
        row_data = saveDoc.toJson(QJsonDocument::Compact);

    }

    qDebug() << row_data;


}

int MyJson::setJsonData(const QString header, const QString config, const bool data){
    QJsonParseError error;
    QJsonDocument saveDoc;
    QJsonDocument loadDoc(QJsonDocument::fromJson(row_data,&error));
    qDebug() << error.errorString();
    //qDebug() << tmp;
    QJsonObject root_obj = loadDoc.object();

    QJsonObject primary = root_obj[header].toObject();
    QJsonValue val = primary.value(config);

    if(val.isBool()){
        primary.insert(config,data);
        root_obj.insert(header,primary);
        saveDoc.setObject(root_obj);
        row_data.clear();
        row_data = saveDoc.toJson(QJsonDocument::Compact);
    }

    //qDebug() << row_data;
}

//int setJsonData(const QString header, const QString config, const bool data);
//int setJsonData(const QString header, const QString config, const QString data);
