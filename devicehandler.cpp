/***************************************************************************
**
** Copyright (C) 2017 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the examples of the QtBluetooth module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "heartrate-global.h"
#include "devicehandler.h"
#include "deviceinfo.h"
#include <QtEndian>
#include <QFile>
#include <QThread>


QUuid serviceUUID("49535343-FE7D-4AE5-8FA9-9FAFD205E455");
QUuid RxUUID("49535343-1E4D-4BD9-BA61-23C647249616"); //rx
QUuid TxUUID("49535343-8841-43f4-a8d4-ecbe34729bb3"); //tx


DeviceHandler::DeviceHandler(QObject *parent) :
    BluetoothBaseClass(parent),
    m_control(0),
    m_service(0),
    m_currentDevice(0),
    writtenCnt(0),
    SendedCnt(0),
    dateMaxSize(0),
    m_foundService(false),
    firstTime(false)
{

    timer = new QTimer;
    connect(timer, &QTimer::timeout, this, &DeviceHandler::timeout);
    timer->start(100);

    delayCommandTimer = new QTimer;
    delayCommandTimer->setSingleShot(true);
    connect(delayCommandTimer, &QTimer::timeout, this, &DeviceHandler::delayCommandTimeout);



    m_myJson = new MyJson;
    connect(m_myJson, &MyJson::JsonRecvDone, this, &DeviceHandler::JsonRecvDone);

}

void DeviceHandler::setAddressType(AddressType type)
{
    switch (type) {
    case DeviceHandler::AddressType::PublicAddress: m_addressType = QLowEnergyController::PublicAddress; break;
    case DeviceHandler::AddressType::RandomAddress: m_addressType = QLowEnergyController::RandomAddress; break;
    }
}

QString DeviceHandler::jsonString() const{
    return m_jsonString;
}
QString DeviceHandler::deviceID() const{
    return m_deviceId;
}
void DeviceHandler::setDeviceID(const QString id){
    m_deviceId = id;
    emit deviceIDChangeed();
}

bool DeviceHandler::connnected() const{
    return m_foundService;
}
int DeviceHandler::grade1_Content() const{
    return content[0];
}
int DeviceHandler::grade2_Content() const{
    return content[1];
}
int DeviceHandler::music_Content() const{
    return content[2];
}

void DeviceHandler::setjsonString(QString json){
    m_jsonString = json;
}
DeviceHandler::AddressType DeviceHandler::addressType() const
{
    if (m_addressType == QLowEnergyController::RandomAddress)
        return DeviceHandler::AddressType::RandomAddress;

    return DeviceHandler::AddressType::PublicAddress;
}

void DeviceHandler::setDevice(DeviceInfo *device)
{
    clearMessages();
    m_currentDevice = device;

    if (m_control) {
        m_control->disconnectFromDevice();
        delete m_control;
        m_control = 0;
    }

    // Create new controller and connect it if device available
    if (m_currentDevice) {

        // Make connections
        m_control = new QLowEnergyController(m_currentDevice->getDevice(), this);
        m_control->setRemoteAddressType(m_addressType);
        connect(m_control, &QLowEnergyController::serviceDiscovered, this, &DeviceHandler::serviceDiscovered);
        connect(m_control, &QLowEnergyController::discoveryFinished, this, &DeviceHandler::serviceScanDone);

        connect(m_control, static_cast<void (QLowEnergyController::*)(QLowEnergyController::Error)>(&QLowEnergyController::error),
                this, [this](QLowEnergyController::Error error) {
            Q_UNUSED(error);
            setError("Cannot connect to remote device.");
            this->disconnectService(false);
        });
        connect(m_control, &QLowEnergyController::connected, this, [this]() {
            setInfo("Controller connected. Search services...");
            m_control->discoverServices();
        });

        connect(m_control, &QLowEnergyController::disconnected, this, [this]() {
            setError("LowEnergy controller disconnected");
            this->disconnectService(false);
        });

        // Connect
        m_control->connectToDevice();

    }
}


void DeviceHandler::serviceDiscovered(const QBluetoothUuid &gatt){
    if (gatt == QBluetoothUuid(serviceUUID)) {
        m_foundService = true;
        emit connectedChangeed();
        firstTime = false;
        content[0] = 3;
        content[1] = 3;
        content[2] = 3;
        emit mycontentChanged();

    }
}

void DeviceHandler::serviceScanDone()
{
    if (m_service) {
        delete m_service;
        m_foundService = 0;
    }

    if (m_foundService){
        m_service = m_control->createServiceObject(QBluetoothUuid(serviceUUID), this);
    }

    if (m_service) {
        connect(m_service, &QLowEnergyService::stateChanged, this, &DeviceHandler::serviceStateChanged);
        connect(m_service, &QLowEnergyService::characteristicChanged, this, &DeviceHandler::updateHeartRateValue);
        connect(m_service, &QLowEnergyService::descriptorWritten, this, &DeviceHandler::confirmedDescriptorWrite);
        connect(m_service, &QLowEnergyService::characteristicWritten, this, &DeviceHandler::characteristicWritten);
        m_service->discoverDetails();
    } else {
        setError("Service not found.");
    }
}

void DeviceHandler::serviceStateChanged(QLowEnergyService::ServiceState s)
{
    switch (s) {
    case QLowEnergyService::DiscoveringServices:
        setInfo(tr("Discovering services..."));
        break;
    case QLowEnergyService::ServiceDiscovered:
    {
        setInfo(tr("Service discovered."));
        const QLowEnergyCharacteristic hrChar = m_service->characteristic(QBluetoothUuid(RxUUID));

        if (!hrChar.isValid()) {
            setError("Charactor not found.");
            break;
        }

        m_notificationDesc = hrChar.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
        if (m_notificationDesc.isValid()){
            m_service->writeDescriptor(m_notificationDesc, QByteArray::fromHex("0100"));
        }
        break;
    }
    default:
        //nothing for now
        break;
    }
}

void DeviceHandler::updateHeartRateValue(const QLowEnergyCharacteristic &c, const QByteArray &value)
{
    if (c.uuid() != QBluetoothUuid(RxUUID))
        return;

    if(!firstTime){
        firstTime = true;
        jsonRefresh();
    }
    m_myJson->loadJSONforString(value);

    emit addrxlog(value);
    setlog(value);

}

void DeviceHandler::confirmedDescriptorWrite(const QLowEnergyDescriptor &d, const QByteArray &value)
{
    if (d.isValid() && d == m_notificationDesc && value == QByteArray::fromHex("0000")) {
        //disabled notifications -> assume disconnect intent
        m_control->disconnectFromDevice();
        delete m_service;
        m_service = 0;
    }

}

void DeviceHandler::characteristicWritten(const QLowEnergyCharacteristic &d, const QByteArray &newValue)
{

    if (d.isValid() && d == m_service->characteristic(QBluetoothUuid(TxUUID))) {
        writtenCnt += newValue.length();
    }
}



void DeviceHandler::disconnectService(const bool val){
    //disable notifications
    if (m_notificationDesc.isValid() && m_service
            && m_notificationDesc.value() == QByteArray::fromHex("0100")) {
        m_service->writeDescriptor(m_notificationDesc, QByteArray::fromHex("0000"));
    } else {
        if (m_control)
            m_control->disconnectFromDevice();

        delete m_service;
        m_service = 0;
    }
    m_foundService = false;
    if(val){
        emit connectedChangeed();
    }else{
        delayCommandTimer->start(10000);
    }
    //emit connectedChangeed();



}
void DeviceHandler::sendBLEData(const QByteArray &value){

    if(m_service){
        int pos = 0;
        int max = value.size();
        int len;
        QByteArray date;
        QLowEnergyCharacteristic characteristic = m_service->characteristic(QBluetoothUuid(TxUUID));

        do{
            len = (max - (pos));
            if(len > 100)
                len = 100;
            date = value.mid(pos,len);
            m_service->writeCharacteristic(characteristic,date);
            pos+=len;
        }while(pos != max);

        SendedCnt += value.length();
    }
}

void DeviceHandler::sendBLEBinaryData(){
    m_myJson->setJsonData("en_us","Contracted",false);
}

void DeviceHandler::sendWJSContracted(const bool val){
    m_myJson->setJsonData("en_us","Contracted",val);
    jsonUpdate();
}

void DeviceHandler::sendWJSContracted(const QString val){
    //qDebug() << "sendWJSContracted string";
    m_myJson->setJsonData("DeviceInfo","SupportContent",val);
    jsonUpdate();

    //content[0] = 3;
    //content[1] = 3;
    content[2] = 3;
    emit mycontentChanged();
    delayCommandTimer->start(5000);


    //QThread::sleep(40);

}

void DeviceHandler::delayCommandTimeout(){


    if(m_foundService)
    {
        jsonRefresh();
    }else{
        emit connectedChangeed();
    }


}

void DeviceHandler::timeout()
{
    return;
    if(!m_service)
        return;

    int size;
    size = SendedCnt - writtenCnt;

    if(size != 0)
        return;

    size = dateMaxSize - SendedCnt;

    if(size){
        QByteArray date;
        QLowEnergyCharacteristic characteristic = m_service->characteristic(QBluetoothUuid(TxUUID));

        if(size > 100)
            size = 100;

        date = blob.mid(SendedCnt,size);
        SendedCnt += size;
        m_service->writeCharacteristic(characteristic,date);

    }

}


void DeviceHandler::jsonRefresh(){
    m_myJson->loadJSON();
    sendBLEData("#RJS#");
}

void DeviceHandler::jsonUpdate(){
    QByteArray tmp;
    tmp.append("#WJS ");
    //tmp.append(m_myJson->getJsonData(m_jsonString));
    tmp.append(m_myJson->getJsonData());
    tmp.append("#");
    sendBLEData(tmp);
}

void DeviceHandler::JsonRecvDone(){
    m_jsonString = m_myJson->getJsonData();
    emit jsonStringChanged();
    //qDebug() << "JsonRecvDone";

    content[0] = m_myJson->getGrade1Content(m_jsonString);
    content[1] = m_myJson->getGrade2Content(m_jsonString);
    content[2] = m_myJson->getMusicContent(m_jsonString);
    emit mycontentChanged();
}
